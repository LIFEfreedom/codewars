#include "Job_1.h"
#include <iostream>
#include <math.h>

using namespace std;


void Job_1 ()
{
	float wSize, pSize;
	int menu = 0;
	
	do
	{
		system ("cls");
		cout << "Job 1 Main menu" << endl << endl;
		cout << "1. Work" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter number menu: "; cin >> menu; cout << endl;

		if (menu == 1)
		{
			cout << "Enter wall size: "; cin >> wSize;
			cout << endl << "Enter pixel size: "; cin >> pSize;

			if (FillFullWall (wSize, pSize))
			{
				cout << "Result: true" << endl;
			}
			else
			{
				cout << "Result: false" << endl;
			}
			system ("pause");
		}
	} while (menu != 0);
}

bool FillFullWall (float wallSize, float pixelSize)
{
	float result = wallSize / pixelSize;
	return (floorf(result) == result) ? true : false;
}