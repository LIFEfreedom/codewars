#pragma once

/*	Task:
*	Your function should take two arguments: the size of the wall in millimeters and the size of a pixel in millimeters.
*	It should return True if you can fit an exact number of pixels on the wall, otherwise it should return False.
*/

void Job_1 ();
bool FillFullWall (float wallSize, float pixelSize);